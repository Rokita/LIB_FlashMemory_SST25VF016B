/*
 * FlashApi.c
 *
 *  Created on: 4 lis 2016
 *      Author: Kamil
 */

#include "../Inc/FlashApi.h"
#include "../Inc/FlashMemory.h"
#include <stdint.h>

#define FLASH_LAST_DATA_ADDRESS_ADDRESS 0x00
#define FLASH_LAST_DATA_ADDRESS_SIZE 0x04
#define FLASH_LAST_DATA_ADDRESS_FIRST_SECTOR_4K 0x00
#define FLASH_LAST_DATA_ADDRESS_SECOND_SECTOR_4K 0x01

#define FLASH_FIRST_DATA_SECTOR_4K 0x02
#define FLASH_FIRST_DATA_ADDRESS 0x00


static uint32_t u32ActualAddress = 0;
/*********************************************************
 *           Declaration of Private Functions
 ********************************************************/

FlashStatus_BusyBit_T bIsFlashBusy();
void vFlashApi_WriteWithAutoIncrement_Parity(uint32_t, uint8_t*, uint16_t);
void vFlashApi_WriteLastDataAddress(uint32_t );

/*********************************************************
 *                Public Functions
 ********************************************************/

uint32_t vFlashApi_Init(void)
   {
   while(Flash_BusyBit_Busy == bIsFlashBusy())
      ;
   uint32_t u32AddressFromFirstSector = 0;
   uint32_t u32AddressFromSecondSector = 0;
   uint8_t u8Address[4];
   vFlashApi_Read((FLASH_LAST_DATA_ADDRESS_FIRST_SECTOR_4K * FLASH_4K_SECTOR_SIZE + FLASH_LAST_DATA_ADDRESS_ADDRESS), u8Address, FLASH_LAST_DATA_ADDRESS_SIZE);
   u32AddressFromFirstSector |= ((uint32_t) u8Address[0]) << 24;
   u32AddressFromFirstSector |= ((uint32_t) u8Address[1]) << 16;
   u32AddressFromFirstSector |= ((uint32_t) u8Address[2]) << 8;
   u32AddressFromFirstSector |= ((uint32_t) u8Address[3]);

   u32AddressFromFirstSector = (u32AddressFromFirstSector < 0x1FFFFF) ? u32AddressFromFirstSector : FLASH_FIRST_DATA_SECTOR_4K * FLASH_4K_SECTOR_SIZE + FLASH_FIRST_DATA_ADDRESS;

   vFlashApi_Read((FLASH_LAST_DATA_ADDRESS_SECOND_SECTOR_4K * FLASH_4K_SECTOR_SIZE + FLASH_LAST_DATA_ADDRESS_ADDRESS), u8Address, FLASH_LAST_DATA_ADDRESS_SIZE);
   u32AddressFromSecondSector |= ((uint32_t) u8Address[0]) << 24;
   u32AddressFromSecondSector |= ((uint32_t) u8Address[1]) << 16;
   u32AddressFromSecondSector |= ((uint32_t) u8Address[2]) << 8;
   u32AddressFromSecondSector |= ((uint32_t) u8Address[3]);

   u32AddressFromSecondSector = (u32AddressFromSecondSector < 0x1FFFFF) ? u32AddressFromSecondSector : FLASH_FIRST_DATA_SECTOR_4K * FLASH_4K_SECTOR_SIZE + FLASH_FIRST_DATA_ADDRESS;

   u32ActualAddress = (u32AddressFromFirstSector > u32AddressFromSecondSector) ? u32AddressFromFirstSector : u32AddressFromSecondSector;

   return u32ActualAddress;
   }

void vFlashApi_getAddress(uint32_t *u32Address)
   {
   *u32Address = u32ActualAddress;
   }

void vFlashApi_Write(uint8_t *pu8Data, uint16_t u16Size)
   {
   if(u16Size % 2 == 0)
      {
      vFlashApi_WriteWithAutoIncrement_Parity(u32ActualAddress, pu8Data, u16Size);
      }
   else
      {
      vFlashApi_WriteWithAutoIncrement_Parity(u32ActualAddress, pu8Data, u16Size - 1);
      //Write the last byte to flash
      vFlashMemory_WriteEnable();
      vFlashMemory_WriteByte(u32ActualAddress + u16Size - 1, pu8Data[u16Size - 1]);
      }
   u32ActualAddress += u16Size;

   vFlashApi_WriteLastDataAddress(u32ActualAddress);
   }

void vFlashApi_Read(uint32_t u32Address, uint8_t *pu8Data, uint16_t u16Size)  // DONE
   {
   while(Flash_BusyBit_Busy == bIsFlashBusy())
      ;
   vFlashMemory_ReadBytes(u32Address, pu8Data, u16Size);
   }

void vFlashApi_ReadStatus(FlashStatus_T *pkFlashStatus)
   {
   vFlashMemory_ReadStatus(pkFlashStatus);
   }

void vFlashApi_ReadId(uint8_t *pu8ManufacturerId, uint8_t *pu8ChipId)
   {
   while(Flash_BusyBit_Busy == bIsFlashBusy())
      ;
   vFlashMemory_ReadId(pu8ManufacturerId, pu8ChipId);
   }

void vFlashApi_SetProtection(void)
   {
   while(Flash_BusyBit_Busy == bIsFlashBusy())
      ;
   vFlashMemory_SetProtectedBlock(Flash_BlockProtected_AllBlocks);
   }

void vFlashApi_ResetProtection(void)
   {
   while(Flash_BusyBit_Busy == bIsFlashBusy())
      ;
   vFlashMemory_SetProtectedBlock(Flash_BlockProtected_None);
   }

void vFlashApi_Erase4kSector(uint16_t u16Sector)
   {
   while(Flash_BusyBit_Busy == bIsFlashBusy())
      ;
   vFlashMemory_WriteEnable();
   vFlashMemory_Erase4kSector(u16Sector);
   }

void vFlashApi_Erase32kSector(uint16_t u16Sector)
   {
   while(Flash_BusyBit_Busy == bIsFlashBusy())
      ;
   vFlashMemory_WriteEnable();
   vFlashMemory_Erase32kSector(u16Sector);
   }

void vFlashApi_Erase64kSector(uint16_t u16Sector)
   {
   while(Flash_BusyBit_Busy == bIsFlashBusy())
      ;
   vFlashMemory_WriteEnable();
   vFlashMemory_Erase64kSector(u16Sector);
   }

void vFlashApi_EraseChip(void)
   {
   while(Flash_BusyBit_Busy == bIsFlashBusy())
      ;
   vFlashMemory_WriteEnable();
   vFlashMemory_EraseChip();
   u32ActualAddress = (FLASH_4K_SECTOR_SIZE * FLASH_FIRST_DATA_SECTOR_4K);
   }

/**********************************************************************
 *                      PRIVATE FUNCTIONS
 *********************************************************************/

FlashStatus_BusyBit_T bIsFlashBusy()
   {
   FlashStatus_T kFlashStatus;
   vFlashApi_ReadStatus(&kFlashStatus);
   return kFlashStatus.eBusyBit;
   }

void vFlashApi_WriteWithAutoIncrement_Parity(uint32_t u32Address, uint8_t* pu8Data, uint16_t u16Size)
   {
   if(u16Size < 2)
      {
      return;
      }

   while(Flash_BusyBit_Busy == bIsFlashBusy())
      ;
   vFlashMemory_WriteEnable();
   vFlashMemory_WriteAutoIncrementFirstBytes(u32Address, pu8Data[0], pu8Data[1]);
   while(Flash_BusyBit_Busy == bIsFlashBusy())
      ;

   for(uint16_t iterator = 2; iterator < u16Size; iterator += 2)
      {
      vFlashMemory_WriteAutoIncrementNextBytes(pu8Data[iterator], pu8Data[iterator + 1]);
      while(Flash_BusyBit_Busy == bIsFlashBusy())
         ;
      }
   vFlashMemory_WriteDisable();
   }

void vFlashApi_WriteLastDataAddress(uint32_t u32Address)
   {
   uint8_t u8Address[4] = {0};

   u8Address[0] = (uint8_t) (u32Address >> 24);
   u8Address[1] = (uint8_t) (u32Address >> 16);
   u8Address[2] = (uint8_t) (u32Address >> 8);
   u8Address[3] = (uint8_t) (u32Address);

   uint32_t u32FirstAddress = (FLASH_LAST_DATA_ADDRESS_FIRST_SECTOR_4K * FLASH_4K_SECTOR_SIZE + FLASH_LAST_DATA_ADDRESS_ADDRESS);
   uint32_t u32SecondAddress = (FLASH_LAST_DATA_ADDRESS_SECOND_SECTOR_4K * FLASH_4K_SECTOR_SIZE + FLASH_LAST_DATA_ADDRESS_ADDRESS);

   vFlashApi_Erase4kSector(FLASH_LAST_DATA_ADDRESS_FIRST_SECTOR_4K);
   vFlashApi_WriteWithAutoIncrement_Parity(u32FirstAddress, u8Address, sizeof(u8Address));
   vFlashApi_Erase4kSector(FLASH_LAST_DATA_ADDRESS_SECOND_SECTOR_4K);
   vFlashApi_WriteWithAutoIncrement_Parity(u32SecondAddress, u8Address, sizeof(u8Address));
   }


