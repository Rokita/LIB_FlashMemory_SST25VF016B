/*
 * FlashMemory.c
 *
 *  Created on: 1 lis 2016
 *      Author: Kamil
 */

#include "../Inc/FlashMemory.h"

#define FLASH_LAST_ADDRESS 0x001FFFFF

#define FLASH_BUSY_MASK 0x01
#define FLASH_WRITE_ENABLE_MASK 0x02
#define FLASH_BLOCK_PROTECTED_MASK 0x3C
#define FLASH_AUTO_INCREMENT_MASK 0x40
#define FLASH_BLOCK_PROTECTION_BIT_MASK 0x80

#define FLASH_BUSY_POS 0x00
#define FLASH_WRITE_ENABLE_POS 0x01
#define FLASH_BLOCK_PROTECTED_POS 0x02
#define FLASH_AUTO_INCREMENT_POS 0x06
#define FLASH_BLOCK_PROTECTION_BIT_POS 0x07

#define READ_COMMAND 0x03
#define ERASE_4K_COMMAND 0x20
#define ERASE_32K_COMMAND 0x52
#define ERASE_64K_COMMAND 0xD8
#define ERASE_CHIP_COMMAND 0x60 // OR 0xC7
#define WRITE_BYTE_COMMAND 0x02
#define AUTO_INCREMENT_COMMAND 0xAD
#define READ_STATUS_COMMAND 0x05
#define ENABLE_WRITE_STATUS_COMMAND 0x50
#define WRITE_STATUS_COMMAND 0x01
#define WRITE_ENABLE_COMMAND 0x06
#define WRITE_DISABLE_COMMAND 0x04
#define READ_ID_COMMAND 0x90 // OR 0xAB
#define READ_JEDEC_ID_COMMAND 0x9F
#define ENABLE_SO_RYBY_COMMAND 0x70
#define DISABLE_SO_RYBY_COMMAND 0x80

#define SECTOR_4K_SIZE 0x00001000
#define SELECTED_4K_SECTOR(sector) (uint32_t) (SECTOR_4K_SIZE * (uint32_t)sector )
#define MAX_SECTOR_4K_COUNT 511

#define SECTOR_32K_SIZE 0x00008000
#define SELECTED_32K_SECTOR(sector) (uint32_t) (SECTOR_32K_SIZE * (uint32_t)sector )
#define MAX_SECTOR_32K_COUNT 63

#define SECTOR_64K_SIZE 0x00010000
#define SELECTED_64K_SECTOR(sector) (uint32_t) (SECTOR_64K_SIZE * (uint32_t)sector )
#define MAX_SECTOR_64K_COUNT 31

void vFlashMemory_EnableWriteStatus(void);
void vFlashMemory_WriteStatusRegister(uint8_t Byte);

///////////////////////////////////////////////////////////////////////////////////
uint8_t Flash_UnpackByte(uint8_t u8Byte, uint8_t u8Mask, uint8_t u8Pos)
///////////////////////////////////////////////////////////////////////////////////
   {
   return ((u8Byte & u8Mask) >> u8Pos);
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_ReadStatus(FlashStatus_T *pkFlashStatus)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint8_t TransmitBuffer[1] = {
   READ_STATUS_COMMAND
   };
   uint8_t ReceivedBuffer[1] = {
   0
   };
   vBoard_MemoryStart();

   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   HAL_SPI_Receive(&hspi1, ReceivedBuffer, sizeof(ReceivedBuffer), 100);

   vBoard_MemoryStop();

   pkFlashStatus->eBusyBit = (FlashStatus_BusyBit_T) Flash_UnpackByte(ReceivedBuffer[0], FLASH_BUSY_MASK, FLASH_BUSY_POS);
   pkFlashStatus->eWriteEnableBit = (FlashStatus_BusyBit_T) Flash_UnpackByte(ReceivedBuffer[0], FLASH_WRITE_ENABLE_MASK, FLASH_WRITE_ENABLE_POS);
   pkFlashStatus->eBlockProtected = (FlashStatus_BusyBit_T) Flash_UnpackByte(ReceivedBuffer[0], FLASH_BLOCK_PROTECTED_MASK, FLASH_BLOCK_PROTECTED_POS);
   pkFlashStatus->eAutoIncrementBit = (FlashStatus_BusyBit_T) Flash_UnpackByte(ReceivedBuffer[0], FLASH_AUTO_INCREMENT_MASK, FLASH_AUTO_INCREMENT_POS);
   pkFlashStatus->eBlockProtectionBitsStatus = (FlashStatus_BusyBit_T) Flash_UnpackByte(ReceivedBuffer[0], FLASH_BLOCK_PROTECTION_BIT_MASK,
   FLASH_BLOCK_PROTECTION_BIT_POS);

   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_WriteEnable(void)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint8_t TransmitBuffer[1] = {
   WRITE_ENABLE_COMMAND
   };
   vBoard_MemoryStart();
   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_WriteDisable(void)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint8_t TransmitBuffer[1] = {
   WRITE_DISABLE_COMMAND
   };
   vBoard_MemoryStart();
   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_Erase4kSector(uint16_t u16Sector)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint32_t u32Address = SELECTED_4K_SECTOR(u16Sector);
   u32Address = u32Address & FLASH_LAST_ADDRESS;
   uint8_t TransmitBuffer[4];
   TransmitBuffer[0] = ERASE_4K_COMMAND;
   TransmitBuffer[1] = (uint8_t) (u32Address >> 16);
   TransmitBuffer[2] = (uint8_t) (u32Address >> 8);
   TransmitBuffer[3] = (uint8_t) u32Address;

   vBoard_MemoryStart();
   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_Erase32kSector(uint16_t u16sector)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint32_t u32Address = SELECTED_32K_SECTOR(u16sector);
   u32Address = u32Address & FLASH_LAST_ADDRESS;
   uint8_t TransmitBuffer[4];
   TransmitBuffer[0] = ERASE_32K_COMMAND;
   TransmitBuffer[1] = (uint8_t) (u32Address >> 16);
   TransmitBuffer[2] = (uint8_t) (u32Address >> 8);
   TransmitBuffer[3] = (uint8_t) u32Address;

   vBoard_MemoryStart();
   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_Erase64kSector(uint16_t u16sector)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint32_t u32Address = SELECTED_64K_SECTOR(u16sector);
   u32Address = u32Address & FLASH_LAST_ADDRESS;
   uint8_t TransmitBuffer[4];
   TransmitBuffer[0] = ERASE_64K_COMMAND;
   TransmitBuffer[1] = (uint8_t) (u32Address >> 16);
   TransmitBuffer[2] = (uint8_t) (u32Address >> 8);
   TransmitBuffer[3] = (uint8_t) u32Address;

   vBoard_MemoryStart();
   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_EraseChip(void)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint8_t TransmitBuffer[1];
   TransmitBuffer[0] = ERASE_CHIP_COMMAND;

   vBoard_MemoryStart();
   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_SetProtectedBlock(FlashStatus_BlockProtected_T eBlockProtected)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint8_t Byte = (uint8_t) (eBlockProtected << FLASH_BLOCK_PROTECTED_POS);

   vFlashMemory_EnableWriteStatus();
   vFlashMemory_WriteStatusRegister(Byte);
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_EnableWriteStatus(void)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint8_t TransmitBuffer[1];
   TransmitBuffer[0] = ENABLE_WRITE_STATUS_COMMAND;

   vBoard_MemoryStart();

   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);

   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_WriteStatusRegister(uint8_t Byte)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint8_t TransmitBuffer[2];
   TransmitBuffer[0] = WRITE_STATUS_COMMAND;
   TransmitBuffer[1] = Byte;

   vBoard_MemoryWP_Enable();
   vBoard_MemoryStart();

   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);

   vBoard_MemoryStop();
   vBoard_MemoryWP_Disable();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_WriteByte(uint32_t u32Address, uint8_t u8Data)
///////////////////////////////////////////////////////////////////////////////////
   {
   u32Address = u32Address & FLASH_LAST_ADDRESS;
   uint8_t TransmitBuffer[5];
   TransmitBuffer[0] = WRITE_BYTE_COMMAND;
   TransmitBuffer[1] = (uint8_t) (u32Address >> 16);
   TransmitBuffer[2] = (uint8_t) (u32Address >> 8);
   TransmitBuffer[3] = (uint8_t) u32Address;
   TransmitBuffer[4] = u8Data;

   vBoard_MemoryStart();
   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_WriteAutoIncrementFirstBytes(uint32_t u32Address, uint8_t u8Data1, uint8_t u8Data2)
///////////////////////////////////////////////////////////////////////////////////
   {
   u32Address = u32Address & FLASH_LAST_ADDRESS;
   uint8_t TransmitBuffer[6];
   TransmitBuffer[0] = AUTO_INCREMENT_COMMAND;
   TransmitBuffer[1] = (uint8_t) (u32Address >> 16);
   TransmitBuffer[2] = (uint8_t) (u32Address >> 8);
   TransmitBuffer[3] = (uint8_t) u32Address;
   TransmitBuffer[4] = u8Data1;
   TransmitBuffer[5] = u8Data2;

   FlashStatus_T kFlashStatus;
   vFlashMemory_ReadStatus(&kFlashStatus);

   vBoard_MemoryStart();
   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_WriteAutoIncrementNextBytes(uint8_t u8Data1, uint8_t u8Data2)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint8_t TransmitBuffer[3];
   TransmitBuffer[0] = AUTO_INCREMENT_COMMAND;
   TransmitBuffer[1] = u8Data1;
   TransmitBuffer[2] = u8Data2;

   vBoard_MemoryStart();
   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_ReadBytes(uint32_t u32Address, uint8_t *pu8Data, uint16_t u16Size)
///////////////////////////////////////////////////////////////////////////////////
   {
   u32Address = u32Address & 0x001FFFFF;
   uint8_t TransmitBuffer[4];
   TransmitBuffer[0] = READ_COMMAND;
   TransmitBuffer[1] = (uint8_t) (u32Address >> 16);
   TransmitBuffer[2] = (uint8_t) (u32Address >> 8);
   TransmitBuffer[3] = (uint8_t) u32Address;

   vBoard_MemoryStart();
   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   HAL_SPI_Receive(&hspi1, pu8Data, u16Size, 1000);
   vBoard_MemoryStop();
   }

///////////////////////////////////////////////////////////////////////////////////
void vFlashMemory_ReadId(uint8_t *pu8ManufacturerId, uint8_t *pu8ChipId)
///////////////////////////////////////////////////////////////////////////////////
   {
   uint8_t TransmitBuffer[4];
   TransmitBuffer[0] = READ_ID_COMMAND;
   TransmitBuffer[1] = 0x00;
   TransmitBuffer[2] = 0x00;
   TransmitBuffer[3] = 0x00;

   uint8_t ReceivedBuffer[4];

   vBoard_MemoryStart();
   HAL_SPI_Transmit(&hspi1, TransmitBuffer, sizeof(TransmitBuffer), 100);
   HAL_SPI_Receive(&hspi1, ReceivedBuffer, sizeof(ReceivedBuffer), 1000);
   vBoard_MemoryStop();

   *pu8ManufacturerId = ReceivedBuffer[0];
   *pu8ChipId = ReceivedBuffer[1];
   }

