/*
 * FlashApi.h
 *
 *  Created on: 4 lis 2016
 *      Author: Kamil
 */

#ifndef STORAGESYSTEM_FLASHMEMORY_FLASHAPI_H_
#define STORAGESYSTEM_FLASHMEMORY_FLASHAPI_H_

#include "../Inc/FlashTypes.h"

#define FLASH_4K_SECTOR_SIZE  0x1000 //4096

uint32_t vFlashApi_Init(void);

void vFlashApi_getAddress(uint32_t *);

void vFlashApi_Write(uint8_t *, uint16_t);
void vFlashApi_Read(uint32_t, uint8_t *, uint16_t);

void vFlashApi_ReadStatus(FlashStatus_T *);
void vFlashApi_SetProtection(void);
void vFlashApi_ResetProtection(void);
void vFlashApi_ReadId(uint8_t *, uint8_t *);

void vFlashApi_Erase4kSector(uint16_t);
void vFlashApi_Erase32kSector(uint16_t);
void vFlashApi_Erase64kSector(uint16_t);
void vFlashApi_EraseChip(void);

#endif /* STORAGESYSTEM_FLASHMEMORY_FLASHAPI_H_ */
