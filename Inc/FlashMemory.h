/*
 * FlashMemory.h
 *
 *  Created on: 1 lis 2016
 *      Author: Kamil
 */

#ifndef STORAGESYSTEM_FLASHMEMORY_FLASHMEMORY_H_
#define STORAGESYSTEM_FLASHMEMORY_FLASHMEMORY_H_

#define ID_OF_MANUFACTURER_SST 0xBF
#define ID_OF_CHIP_SST25VF016B 0x41

//#include "Board/board.h"

#include "../Inc/FlashTypes.h"



void vFlashMemory_ReadStatus(FlashStatus_T *);
void vFlashMemory_WriteEnable(void);
void vFlashMemory_WriteDisable(void);
void vFlashMemory_Erase4kSector(uint16_t);
void vFlashMemory_Erase32kSector(uint16_t);
void vFlashMemory_Erase64kSector(uint16_t);
void vFlashMemory_EraseChip(void);

void vFlashMemory_SetProtectedBlock(FlashStatus_BlockProtected_T);
void vFlashMemory_WriteByte(uint32_t , uint8_t);
void vFlashMemory_WriteAutoIncrementFirstBytes(uint32_t, uint8_t , uint8_t);
void vFlashMemory_WriteAutoIncrementNextBytes(uint8_t , uint8_t);
void vFlashMemory_ReadBytes(uint32_t , uint8_t *, uint16_t );
void vFlashMemory_ReadId(uint8_t *, uint8_t *);

#endif /* STORAGESYSTEM_FLASHMEMORY_FLASHMEMORY_H_ */
