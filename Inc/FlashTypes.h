/*
 * FlashTypes.h
 *
 *  Created on: 4 lis 2016
 *      Author: Kamil
 */

#ifndef STORAGESYSTEM_FLASHMEMORY_FLASHTYPES_H_
#define STORAGESYSTEM_FLASHMEMORY_FLASHTYPES_H_

#include <stdint.h>

typedef enum FlashStatus_BusyBit
   {
   Flash_BusyBit_NotBusy,
   Flash_BusyBit_Busy,
   Flash_BusyBit_LastUnused
   } FlashStatus_BusyBit_T;

typedef enum FlashStatus_WriteEnableBit
   {
   Flash_WriteEnableBit_Disable,
   Flash_WriteEnableBit_Enable,
   Flash_WriteEnableBit_LastUnused
   } FlashStatus_WriteEnableBit_T;

typedef enum FlashStatus_BlockProtected
   {
   Flash_BlockProtected_None,
   Flash_BlockProtected_Upper32,
   Flash_BlockProtected_Upper16,
   Flash_BlockProtected_Upper8,
   Flash_BlockProtected_Upper4,
   Flash_BlockProtected_Upper2,
   Flash_BlockProtected_AllBlocks,
   Flash_BlockProtected_AllBlocks2,
   Flash_BlockProtected_LastUnused
   } FlashStatus_BlockProtected_T;

typedef enum FlashStatus_AutoIncrement
   {
   Flash_AutoIncrement_Off,
   Flash_AutoIncrement_On,
   Flash_AutoIncrement_LastUnused,
   } FlashStatus_AutoIncrement_T;

typedef enum FlashStatus_BlocksProtectionBitsStatus
   {
   Flash_BlocksProtectionBits_ReadOnly,
   Flash_BlocksProtectionBits_ReadWrite,
   Flash_BlocksProtectionBits_LastUnused
   } FlashStatus_BlockProtectionBitsStatus_T;

typedef struct FlashStatus
   {
      FlashStatus_BusyBit_T eBusyBit;
      FlashStatus_WriteEnableBit_T eWriteEnableBit;
      FlashStatus_BlockProtected_T eBlockProtected;
      FlashStatus_AutoIncrement_T eAutoIncrementBit;
      FlashStatus_BlockProtectionBitsStatus_T eBlockProtectionBitsStatus;
   } FlashStatus_T;

#endif /* STORAGESYSTEM_FLASHMEMORY_FLASHTYPES_H_ */
